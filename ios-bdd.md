# BDD for iOS

## One Time Only
1. Install [Cocopod](https://cocoapods.org) via [Homebrew](https://brew.sh)
```
brew install cocoapods
```
1. Install [XCFit](https://github.com/XCTEQ/XCFit)
```
sudo gem install xcfit
```
1. Setup xcfit templates into Xcode
```
xcfit setup_xcode_templates
```

## Create Xcode project and add Cucumber Bundle to it

![Create Xcode Project](Create_Xcode_App.mp4)

## Setup the Podfile
![Create Podfile](Create_Podfile.mp4)

## Install [Cucumberish](https://cocoapods.org/pods/Cucumberish) via Cocopods
![Install Cucumberish](Install_Cucumberish.mp4)

## Create a Features folder called `CucumbertTests`
![Create Feature Folder](Create_Features_Folder.mp4)

## Open Xcode via the `<app_name>.xcworkspace` file
![Open Xcode via XCWorkspace](Open_XCWorkspace.mp4)

## Make sure the Defines Module Build Setting is set to `Yes` for your test target under `Build Settings > All & Combines > Packaging`
![Set Define Modules Build Setting](Define_Module_Build_Setting.mp4)

## Remove the duplicate build tasks created in error
![Remove Duplicate Build Tasks](Remove_Bad_Build_Phases.mp4)

## Drag the `Features` folder created above from the macOS Finder to inside the Xcode Cucumber test group (you’ll use this in a minute to create feature files). Make sure it’s just a folder and not an Xcode group.
![Add Features folder to Xcode Project](Add_Feature_Folder_to_Cucumber.mp4)

## Fix the missing tag in `<project_name>Cucumber.swift` file under the `Cucumber` group folder
Add `@objc` to the top class definition and the class init function.

![Fix a Missing Tag](Fix_Missing_tag.mp4)

## Create feature file under the `Features` directory

## Create directories from the Finder for:
- Screens
- Step Definitions
- Common
- Supporting Files

![Create Subdirectories to Better Organize Code](Create_Directories.mp4)

## Move these files from the root of the `Cucumber Tests` directory into there respective sub directories
- Screens
    - HomeScreen.swift
- Step Definitions
    - HomeScreenSteps.swift
- Common
    - CommonStepDefinitions.swift
- Supporting Files
    - <project_name>CucumberTests-Bridging-Header.h
    - <project_name>CucumberTests.h
    - <project_name>CucumberTests.m

![Reorganize Files into New Subdirectories](Move_Files_to_Folders.mp4)

## Fix all the broken file links under the `Cucumber Tests` group for the sub directories and their associated files.
![Fix Broken File Links in Xcode Project](Fix_Broken_File_Links.mp4)

## Fix the typos in the `Common > CommonStepDefinitions.swift` file
- Line 1: Remove “ery” from beginning of line
- Line 23: Rename var “elementQurey” to “elementQuery”
- Line 37: Rename “elementQurey” to “elementQuery”
- Line 102: Change UInt to Int in both sides of the equals
- Line 110: Change UInt to Int in both sides of the equals

![Fix Typos in CommonStepDefinitions.swift File](Fix_Typos.mp4)

## Fix broken path to Bridging Header
![Fix Swift Compiler General Broken File Path](Fix_Broken_Bridging_Header.mp4)
